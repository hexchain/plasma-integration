# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
# Vincenzo Reale <smart2128vr@gmail.com>, 2014, 2022.
#
msgid ""
msgstr ""
"Project-Id-Version: frameworkintegration5\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-11-23 01:39+0000\n"
"PO-Revision-Date: 2022-11-06 01:31+0100\n"
"Last-Translator: Vincenzo Reale <smart2128vr@gmail.com>\n"
"Language-Team: Italian <kde-i18n-it@kde.org>\n"
"Language: it\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 22.08.3\n"

#: platformtheme/kdeplatformfiledialoghelper.cpp:306
#, kde-format
msgctxt "@title:window"
msgid "Open File"
msgstr "Apri file"

#: platformtheme/kdeplatformfiledialoghelper.cpp:307
#, kde-format
msgctxt "@title:window"
msgid "Save File"
msgstr "Salva file"

#: platformtheme/kdeplatformtheme.cpp:401
#, kde-format
msgctxt "@action:button"
msgid "Save All"
msgstr "Salva tutto"

#: platformtheme/kdeplatformtheme.cpp:405
#, kde-format
msgctxt "@action:button"
msgid "&Yes"
msgstr "&Sì"

#: platformtheme/kdeplatformtheme.cpp:407
#, kde-format
msgctxt "@action:button"
msgid "Yes to All"
msgstr "Sì a tutto"

#: platformtheme/kdeplatformtheme.cpp:409
#, kde-format
msgctxt "@action:button"
msgid "&No"
msgstr "&No"

#: platformtheme/kdeplatformtheme.cpp:411
#, kde-format
msgctxt "@action:button"
msgid "No to All"
msgstr "No a tutto"

#: platformtheme/kdeplatformtheme.cpp:414
#, kde-format
msgctxt "@action:button"
msgid "Abort"
msgstr "Interrompi"

#: platformtheme/kdeplatformtheme.cpp:416
#, kde-format
msgctxt "@action:button"
msgid "Retry"
msgstr "Riprova"

#: platformtheme/kdeplatformtheme.cpp:418
#, kde-format
msgctxt "@action:button"
msgid "Ignore"
msgstr "Ignora"

#: platformtheme/kdirselectdialog.cpp:122
#, kde-format
msgctxt "folder name"
msgid "New Folder"
msgstr "Nuova cartella"

#: platformtheme/kdirselectdialog.cpp:128
#, kde-format
msgctxt "@title:window"
msgid "New Folder"
msgstr "Nuova cartella"

#: platformtheme/kdirselectdialog.cpp:129
#, kde-format
msgctxt "@label:textbox"
msgid ""
"Create new folder in:\n"
"%1"
msgstr ""
"Crea un nuova cartella in:\n"
"%1"

#: platformtheme/kdirselectdialog.cpp:160
#, kde-format
msgid "A file or folder named %1 already exists."
msgstr "Un file o una cartella con nome %1 esiste già."

#: platformtheme/kdirselectdialog.cpp:169
#, kde-format
msgid "You do not have permission to create that folder."
msgstr "Non disponi dei permessi per creare la cartella."

#: platformtheme/kdirselectdialog.cpp:271
#, kde-format
msgctxt "@title:window"
msgid "Select Folder"
msgstr "Seleziona cartella"

#: platformtheme/kdirselectdialog.cpp:279
#, kde-format
msgctxt "@action:button"
msgid "New Folder..."
msgstr "Nuova cartella..."

#: platformtheme/kdirselectdialog.cpp:336
#, kde-format
msgctxt "@action:inmenu"
msgid "New Folder..."
msgstr "Nuova cartella..."

#: platformtheme/kdirselectdialog.cpp:345
#, kde-format
msgctxt "@action:inmenu"
msgid "Move to Trash"
msgstr "Cestina"

#: platformtheme/kdirselectdialog.cpp:354
#, kde-format
msgctxt "@action:inmenu"
msgid "Delete"
msgstr "Elimina"

#: platformtheme/kdirselectdialog.cpp:365
#, kde-format
msgctxt "@option:check"
msgid "Show Hidden Folders"
msgstr "Mostra cartelle nascoste"

#: platformtheme/kdirselectdialog.cpp:372
#, kde-format
msgctxt "@action:inmenu"
msgid "Properties"
msgstr "Proprietà"

#: platformtheme/kfiletreeview.cpp:185
#, kde-format
msgid "Show Hidden Folders"
msgstr "Mostra cartelle nascoste"

#~ msgid "Opening..."
#~ msgstr "Apertura in corso..."

#~ msgid "Saving..."
#~ msgstr "Salvataggio in corso..."
