# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Martin Schlander <mschlander@opensuse.org>, 2016.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-11-23 01:39+0000\n"
"PO-Revision-Date: 2016-06-18 14:51+0100\n"
"Last-Translator: Martin Schlander <mschlander@opensuse.org>\n"
"Language-Team: Danish <kde-i18n-doc@kde.org>\n"
"Language: da\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 2.0\n"

#: platformtheme/kdeplatformfiledialoghelper.cpp:306
#, kde-format
msgctxt "@title:window"
msgid "Open File"
msgstr "Åbn fil"

#: platformtheme/kdeplatformfiledialoghelper.cpp:307
#, kde-format
msgctxt "@title:window"
msgid "Save File"
msgstr "Gem fil"

#: platformtheme/kdeplatformtheme.cpp:401
#, kde-format
msgctxt "@action:button"
msgid "Save All"
msgstr "Gem alt"

#: platformtheme/kdeplatformtheme.cpp:405
#, kde-format
msgctxt "@action:button"
msgid "&Yes"
msgstr ""

#: platformtheme/kdeplatformtheme.cpp:407
#, kde-format
msgctxt "@action:button"
msgid "Yes to All"
msgstr "Ja til alle"

#: platformtheme/kdeplatformtheme.cpp:409
#, kde-format
msgctxt "@action:button"
msgid "&No"
msgstr ""

#: platformtheme/kdeplatformtheme.cpp:411
#, kde-format
msgctxt "@action:button"
msgid "No to All"
msgstr "Nej til alle"

#: platformtheme/kdeplatformtheme.cpp:414
#, kde-format
msgctxt "@action:button"
msgid "Abort"
msgstr "Afbryd"

#: platformtheme/kdeplatformtheme.cpp:416
#, kde-format
msgctxt "@action:button"
msgid "Retry"
msgstr "Prøv igen"

#: platformtheme/kdeplatformtheme.cpp:418
#, kde-format
msgctxt "@action:button"
msgid "Ignore"
msgstr "Ignorér"

#: platformtheme/kdirselectdialog.cpp:122
#, kde-format
msgctxt "folder name"
msgid "New Folder"
msgstr "Ny mappe"

#: platformtheme/kdirselectdialog.cpp:128
#, kde-format
msgctxt "@title:window"
msgid "New Folder"
msgstr "Ny mappe"

#: platformtheme/kdirselectdialog.cpp:129
#, kde-format
msgctxt "@label:textbox"
msgid ""
"Create new folder in:\n"
"%1"
msgstr ""
"Opret ny mappe i:\n"
"%1"

#: platformtheme/kdirselectdialog.cpp:160
#, kde-format
msgid "A file or folder named %1 already exists."
msgstr "En fil eller mappe ved navn %1 eksisterer allerede."

#: platformtheme/kdirselectdialog.cpp:169
#, kde-format
msgid "You do not have permission to create that folder."
msgstr "Du har ikke rettigheder til at oprette den mappe."

#: platformtheme/kdirselectdialog.cpp:271
#, kde-format
msgctxt "@title:window"
msgid "Select Folder"
msgstr "Vælg mappe"

#: platformtheme/kdirselectdialog.cpp:279
#, kde-format
msgctxt "@action:button"
msgid "New Folder..."
msgstr "Ny mappe..."

#: platformtheme/kdirselectdialog.cpp:336
#, kde-format
msgctxt "@action:inmenu"
msgid "New Folder..."
msgstr "Ny mappe..."

#: platformtheme/kdirselectdialog.cpp:345
#, kde-format
msgctxt "@action:inmenu"
msgid "Move to Trash"
msgstr "Flyt til papirkurv"

#: platformtheme/kdirselectdialog.cpp:354
#, kde-format
msgctxt "@action:inmenu"
msgid "Delete"
msgstr "Slet"

#: platformtheme/kdirselectdialog.cpp:365
#, kde-format
msgctxt "@option:check"
msgid "Show Hidden Folders"
msgstr "Vis skjulte mapper"

#: platformtheme/kdirselectdialog.cpp:372
#, kde-format
msgctxt "@action:inmenu"
msgid "Properties"
msgstr "Egenskaber"

#: platformtheme/kfiletreeview.cpp:185
#, kde-format
msgid "Show Hidden Folders"
msgstr "Vis skjulte mapper"

#~ msgid "Opening..."
#~ msgstr "Åbner..."

#~ msgid "Saving..."
#~ msgstr "Gemmer..."
